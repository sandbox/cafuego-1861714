<?php

/**
 * @file commerce_ordernumber_handler_commerce_ordernumber_sort.inc
 * Provides a numeric sort handler for a varchar field.

/**
 * Override the query() method on the standard views sort handler and leave
 * everything else untouched.
 */
class commerce_ordernumber_handler_commerce_ordernumber_sort extends views_handler_sort {

  /**
   * Append a '+ 0' to the field we're ordering by to enforce a numeric
   * sort even if the field is a varchar.
   *
   * Do not pass a table name, instead we construct a full (aliased)
   * table.field string so as to not upset the SQL server in case of
   * multiple orderid fields.
   */
  function query() {
    $this->ensure_my_table();
    $order_override = $this->table_alias . '.' . $this->real_field . ' + 0';
    $this->query->add_orderby(NULL, $order_override, $this->options['order']);
  }
}
